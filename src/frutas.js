//import { writable } from 'svelte/store';

class Fruta {
    
    // public _nombre
    // public _emoji
    // public _id

    constructor(nombre, emoji,color,id){
        this._nombre = nombre
        //console.log("NOMBRE",nombre)
        this._emoji = emoji
        //console.log("EMOJI",emoji)
        this._color = color
        this._id = id
        //console.log("ID", id)
        this._cantidad = 0
        }
      
   json() {
       return JSON.stringify(this)
        }
            
   add(){
       this._cantidad++
        //console.log("AGREGANDO",this.cantidad)
       } 
       
    subs(){
        this._cantidad -= 1
        }
        
    repr(){
        return this.nombre.toUpperCase()
        }

    get nombre(){
        return this._nombre
    }

    get emoji(){
        return this._emoji
    }

    get color(){
        return this._color
    }

    get id(){
        return this._id
    }

    get cantidad(){
        return this._cantidad
    }

    set nombre(valor){
        this._nombre = valor        
    }

    set emoji(valor){
        this._emoji = valor
    }

    set color(valor){
        this._color = valor
    }

    set id(valor){
        this._id = valor
    }

    set cantidad(valor){
        this._cantidad = valor
    }
}

export class Frutero {
    constructor(frutas) {
        this.frutas = new Map();
        frutas.map( (fruta, index) => {
                    //console.log("INDEX", index)
                    //console.log("fruta",fruta)
                    //console.log("emoji",emoji)
                    this.frutas.set(fruta, new Fruta(...fruta,index))
            })
        }
        
        show(){
            this.frutas.forEach((key, value)=> {
                    console.log(key,"->", value)
                })
            }
            
        add_fruit(nombre_fruta){
            // obtenemos el objeto fruta
           let fruta = this.frutas.get(nombre_fruta)
           if (!typeof fruta === 'undefined') {
            fruta.add()
           }
        
            }

        *[Symbol.iterator](){
            yield this.frutas
        }
    }


const nombres_frutas =  ["Cambur","Manzana", "Frutilla", "Melón"]
let frutas = new Frutero(nombres_frutas)
// sumar un Cambur
frutas.add_fruit("Cambur")



